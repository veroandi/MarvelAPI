<?php

namespace AppBundle\Controller;

use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function indexAction()
    {
        $timestamp     = time();
        $publicApiKey  = $this->getParameter('api_public_key');
        $privateApiKey = $this->getParameter('api_private_key');
        $hash          = md5($timestamp . $privateApiKey . $publicApiKey);
        $baseUrl       = "https://gateway.marvel.com:443/v1/public/characters";
        $url           = $baseUrl . '?' .'ts=' . $timestamp . '&apikey=' . $publicApiKey . '&hash=' . $hash;

        $client     = new Client();
        $req        = $client->request('GET', $url);
        $characters = $req->getBody()->getContents();
        $obj        = json_decode($characters, true);
        $results    = $obj['data']['results'];

        return $this->render('default/index.html.twig', [
            'results' => $results,
        ]);
    }
}
